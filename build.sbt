name := "PizzaOptimizer"

version := "1.0"

scalaVersion := "2.11.7"

scalaSource in Compile := baseDirectory.value / "src"

mainClass in (Compile, run) := Some("pizza.OrderProcessor")
