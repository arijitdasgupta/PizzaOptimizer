package pizza

import java.io.{BufferedReader, InputStream, InputStreamReader, PrintStream}

class LineParseError extends Exception {}

trait Order {
  val orderTime:Long
  val orderLength:Long
}

class OrderItem(val orderTime:Long, val orderLength:Long) extends Order {}

class BlankTick(val orderTime:Long) extends Order {
  val orderLength:Long = 0
}

trait Oven {
  val outStandingOrders: List[Order]
  val currentTime: Long
  val nOfOrders: Int
  val timeToCook: Long
  val lastOrderLength: Long
  val totalWaitTime: Long
}

class OvenState(val outStandingOrders: List[Order],
                val currentTime: Long,
                val nOfOrders: Int,
                val timeToCook: Long,
                val lastOrderLength: Long,
                val totalWaitTime: Long) extends Oven {}

object DefaultOvenState extends Oven {
  val outStandingOrders: List[Order] = List[Order]()
  val currentTime: Long = 0
  val nOfOrders: Int = 0
  val timeToCook: Long = 0
  val lastOrderLength: Long = 0
  val totalWaitTime: Long = 0
}

object OrderProcessorUtils {
  /**
    * Parses a single order item from string
    *
    * @param orderLine
    * @return OrderItem
    */
  def parseSingleOrderLine(orderLine: String): Order = {
    val numberStrings: Array[String] = orderLine.split(" ")
    val numbers: Array[Long] = numberStrings.take(2).map((numberStr: String) => {
      try {
        numberStr.toLong
      } catch {
        case _: Exception => throw new LineParseError
      }
    })

    if (numbers.length == 2) new OrderItem(numbers(0), numbers(1)) else throw new LineParseError
  }

  /**
    * selects the fastest order
    *
    * @param orders
    * @return
    */
  def pickFastestOrder(orders: List[Order]): Order = {
    orders.reduce((order1, order2) => if (order1.orderLength > order2.orderLength) order2 else order1)
  }

  /**
    * Removes a specific order
    *
    * @param orders
    * @param orderToRemove
    * @return
    */
  def removeOrder(orders: List[Order], orderToRemove: Order): List[Order] = {
    orders.filter(order => order != orderToRemove)
  }

  /**
    * Picks the quickest order out of all
    *
    * @param orders
    * @return
    */
  def pluckFastestOrder(orders: List[Order]):(Order, List[Order]) = {
    val fastestOrder = pickFastestOrder(orders)
    (fastestOrder, removeOrder(orders, fastestOrder))
  }

  /**
    * Creates a new oven state against e new order,
    * This contains the main flow logic
    *
    * @param ovenState
    * @param order
    * @return
    */
  def reduceOrder(ovenState: Oven, order: Order):Oven = {
    val elapsedTime = order.orderTime - ovenState.currentTime

    val newOrdersList = order match {
      case _:OrderItem => ovenState.outStandingOrders :+ order
      case _:BlankTick => ovenState.outStandingOrders
    }

    if (ovenState.timeToCook > 0) {
      new OvenState(newOrdersList,
        order.orderTime,
        ovenState.nOfOrders,
        ovenState.timeToCook - elapsedTime,
        ovenState.lastOrderLength,
        ovenState.totalWaitTime + elapsedTime)
    }
    else {
      val newOrderTuple = pluckFastestOrder(newOrdersList)

      new OvenState(newOrderTuple._2,
        order.orderTime,
        ovenState.nOfOrders + 1,
        newOrderTuple._1.orderLength,
        newOrderTuple._1.orderLength,
        ovenState.totalWaitTime + ovenState.lastOrderLength - elapsedTime)
    }
  }

  /**
    * Recursively tick the reducer with blank order till all outstanding orders vanish
    * This routine can be optimized
    *
    * @param ovenState
    * @return
    */
  def bakeRecursive(ovenState: Oven):Oven = {
    ovenState.outStandingOrders.length match {
      case 0 => // Loopig till
        if (ovenState.timeToCook > 0)
          bakeRecursive(reduceOrder(ovenState, new BlankTick(ovenState.currentTime + ovenState.lastOrderLength)))
        else ovenState
      case _ => bakeRecursive(reduceOrder(ovenState, new BlankTick(ovenState.currentTime + 1))) // The increment can be optimized
    }
  }

  /** Calculates minimum avg waiting time
    *
    * @param orderStream
    * @return
    */
  def calculateOrdersForMinimumAvgWait(orderStream: Stream[Order]):Long = {
    val ovenState = orderStream.foldLeft(DefaultOvenState.asInstanceOf[Oven])(reduceOrder)
    val finalOvenState = bakeRecursive(ovenState)
    finalOvenState.totalWaitTime/finalOvenState.nOfOrders
  }
}

object OrderProcessor {
  /**
    * The processor...
    *
    * @param in
    * @param out
    * @return
    */
  def process(in: InputStream, out: PrintStream) = {
    val inputReader = new InputStreamReader(in)
    val buffReader = new BufferedReader(inputReader)
    val lineReadStream = Stream.continually(buffReader.readLine)

    // Getting the number of orders
    val nOfTotalOrders:Int = lineReadStream.take(1).foldLeft(0)((_, ip:String) => ip.toInt)

    // Processing the order items
    val orderStream = lineReadStream.drop(1)
      .take(nOfTotalOrders)
      .map((line:String) => {
        try { // Ignores a bad line...
          OrderProcessorUtils.parseSingleOrderLine(line)
        } catch {
          case e:LineParseError => None
        }
      })
      .filter(item => item != None).asInstanceOf[Stream[Order]]

    val minimumAvgWait:Long = OrderProcessorUtils.calculateOrdersForMinimumAvgWait(orderStream)

    out.print(minimumAvgWait)
  }

  /**
    * While running in main context
    *
    * @param args
    * @return
    */
  def main(args: Array[String]) = {
    println("Your inputs please...")
    try {
      process(System.in, System.out)
    } catch {
      case e:Exception => {
        println("Something went wrong")
        println(e.toString())
        System.exit(1)
      }
    }
  }
}